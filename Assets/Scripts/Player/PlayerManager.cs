using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;

public class PlayerManager : MonoBehaviour
{
    public static int playerHP = 100;
    public TextMeshProUGUI playerHPText;
    public static bool isGameOver;
    public GameObject bloodOverlay;
    void Start()
    {
        isGameOver = false;
        playerHP = 100;
    }

    // Update is called once per frame
    void Update()
    {
        playerHPText.text = "+" + playerHP;

        if (isGameOver)
        {
            SceneManager.LoadScene("Level");
        }
    }

    public IEnumerator TakeDamage(int damageAmount)
    {
        bloodOverlay.SetActive(true);
        playerHP -= damageAmount;

        if (playerHP <= 0)
        {
            isGameOver = true;
        }

        yield return new WaitForSeconds(1.5f);
        bloodOverlay.SetActive(false);
    }
}
